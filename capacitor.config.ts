import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Dev Roadmaps',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
