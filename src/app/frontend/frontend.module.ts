import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FrontendPageRoutingModule } from './frontend-routing.module';

import { FrontendPage } from './frontend.page';
import { RoadmapModule } from '../basicComponents/roadmap/roadmap.module';
import { ChildrenModule } from '../basicComponents/children/children.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FrontendPageRoutingModule,
    RoadmapModule,
    ChildrenModule
  ],
  declarations: [FrontendPage]
})
export class FrontendPageModule {}
