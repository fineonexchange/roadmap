import { Component, OnInit, ViewChild  } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RoadmapComponent } from '../basicComponents/roadmap/roadmap.component';

@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.page.html',
  styleUrls: ['./frontend.page.scss'],
})
export class FrontendPage implements OnInit {
  @ViewChild(RoadmapComponent ) child: RoadmapComponent;
  frontEndArr = [];
  children = [];
  hideHeaderBool;
  constructor(private dataService: DataService,
  private activatedRoute: ActivatedRoute,
  private router: Router) { }

  ngOnInit() {
  this.getFrontEndArr();
  }

  hideHeader(){
  this.hideHeaderBool = true;
  }

  showHeader(){
    setTimeout(()=>{
      this.hideHeaderBool = false;
    },400);
    }

  ionViewWillEnter(){
   this.child.initiate();
  }

   goTo(ev){
   this.router.navigate(['/children','frontend',`${ev.key}`,`${ev.id}`]);
  }


  getFrontEndArr(){
    const frontEnd = this.dataService.frontEndData;
    Object.keys(frontEnd).forEach((key, index) => {
      frontEnd[key].id = 'fo_' + index;
      this.frontEndArr.push(frontEnd[key]);
    });

  }

}
