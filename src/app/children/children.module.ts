import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChildrenPageRoutingModule } from './children-routing.module';
import { ChildrenModule } from '../basicComponents/children/children.module';

import { ChildrenPage } from './children.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChildrenModule,
    ChildrenPageRoutingModule
  ],
  declarations: [ChildrenPage]
})
export class ChildrenPageModule {}
