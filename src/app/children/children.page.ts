import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-children-page',
  templateUrl: './children.page.html',
  styleUrls: ['./children.page.scss'],
})
export class ChildrenPage implements OnInit {
 children = [];
 title;
 id;
 category;
 childrenText;
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    await this.dataService.loadProgress();
    this.activatedRoute.params.subscribe(params => {
      const key = params.parentKey;
      this.category = params.category;
      this.id = params.parentId;
      if(key){
       this.revealChildren(key);
      }});
  }


  revealChildren(key) {
    const dic = {
      frontend:'frontEndData',
    };
    const parent = this.dataService[dic[this.category]][key];
    this.title = parent.title;
    this.children = parent.children;
    if(parent.isAtLeastOne) {
      this.childrenText = 'Master at least one item, once you finish it, tick it here.';
    }
    else{
      this.childrenText = `You must complete all items. Tick an item once you're done learning it.`;
    }

    this.children.forEach(e => {
      const margedId = this.id + '_' + e.id;
      e.done = this.dataService.progress[margedId] || false;
    });
    }
}
