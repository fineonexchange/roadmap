import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  guidesArr = [];
  frontEndArr = [];
  mainMode: boolean;
  frontEndMode: boolean;
  backendMode: boolean;
  devOpsMode: boolean;
  reactMode: boolean;
  angularMode: boolean;

  constructor(
    private router: Router,
    private dataService: DataService,
  ) { }

  ngOnInit() {
    this.mainMode = true;
    this.guidesArr = this.dataService.guidesData.guidesArr;
  }

  guideClicked(val){
    if(val === 'frontend'){
      this.router.navigate(['/frontend']);
    }
  }




}
