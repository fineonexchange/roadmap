/* eslint-disable @typescript-eslint/naming-convention */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';


@Injectable({
  providedIn: 'root'
})
export class DataService {
public progress  = {};
  httpOptions = {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
  };
  guidesUrl = `https://myjson.dit.upm.es/api/bins/2x8x`;


 guidesData = {
  guidesArr: [
    {
      key:'frontend',
      title: 'Frontend',
      desc: 'Step by step guide to becoming a modern frontend developer in 2021',
      color: 'navy'
    },
    {
      key:'backend',
      title: 'Backend',
      desc: 'Step by step guide to becoming a modern backend developer in 2021',
      color: 'red'
    },
    {
      key:'devops',
      title: 'DevOps',
      desc: 'Step by step guide for DevOps, SRE or any other Operations Role in 2021',
      color: 'darkGreen'
    },
    {
      key:'react',
      title: 'React',
      desc: 'Step by step guide to becoming a React developer in 2021',
      color: 'blueishGreen'
    },
    {
      key:'angular',
      title: 'Angular',
      desc: 'Step by step guide to becoming an Angular developer in 2021',
      color: 'blue'
    }
  ]
};



internetBasics = {
internet:{
  title:'Internet',
  key:'internet',
  group:'internet',
  children: [
    {order:0, id:'child_0', desc: 'How does the internet work?'},
    {order:1, id:'child_1', desc:'What is http?'},
    {order:2, id:'child_2', desc:'Browsers and how they work?'},
    {order:3, id:'child_3', desc: 'DNS and how it works?'},
    {order:4, id:'child_4', desc: 'What is domain name?'},
    {order:5, id:'child_5', desc: 'What is hosting?'},
   ]
}
};


htmlBasics = {
  html:
    {
      title:'html',
      key:'html',
      group:'htmlCssJs',
      children: [
        {order:0,id:'child_0', desc: 'Learn the basics'},
        {order:1,id:'child_1', desc:'Writing Semantic Html'},
        {order:2,id:'child_2', desc:'Forms and Validations'},
        {order:3,id:'child_3', desc: 'Conventions and Best Practices'},
        {order:4,id:'child_4', desc: 'Accessibility'},
        {order:5,id:'child_5', desc: 'SEO Basics'},
       ],
    }
  };

  cssBasics = {
    css: {
      title: 'css',
      key:'css',
      group:'htmlCssJs',
      children:[
        {order:0,id:'child_0', desc: 'Learn the basics'},
        {order:1,id:'child_1', desc: 'Making layouts (Floats, Positioning, Display, Box Model, CSS Grid, FlexBox)'},
        {order:2,id:'child_2', desc: 'Responsive Design & Media Queries'},
      ]
    }
  };

  jsBasics = {
    js: {
      title: 'js',
      key:'js',
      group:'htmlCssJs',
      children:[
        {order:0,id:'child_0', desc: 'Syntax and basic constructs'},
        {order:1,id:'child_1', desc: 'Learn DOM Manipulation'},
        {order:2,id:'child_2', desc: 'Learn Fetch API/AJAX (XHR)'},
        {order:3,id:'child_3', desc: 'ES6+ and Module Javascript'},
        {order:4,id:'child_4', desc: 'Understand concepts: Hoisting, Scope, Event Bubbling, Prototype, Shadow DOM, strict'},
      ]
    }
  };

  versionControlSystems = {
    versionControlSystems:{
       title:'Version Control Systems',
       key: 'versionControlSystems',
       group:'versionAndSecurity',
       children:[
        {order:0,id:'child_0', desc: 'Basic Usage of Git'},
        {order:1,id:'child_1', desc: 'Create account and learn to use GitHub (Or GitLab or Bitbucket)'},
       ]
    }
  };


  webSecurityKnowledge = {
    webSecurityKnowledge:{
       title:'Web Security Knowledge',
       isAtLeastBasic:true,
       key: 'webSecurityKnowledge',
       group:'versionAndSecurity',
       children: [
        {order:0,id:'child_0', desc: 'HTTPS'},
        {order:1,id:'child_1', desc:'Content Security Policy'},
        {order:2,id:'child_2', desc:'CORS'},
        {order:3,id:'child_3', desc: 'Conventions and Best Practices'},
        {order:4,id:'child_4', desc: 'Accessibility'},
        {order:5,id:'child_5', desc: 'SEO Basics'},
       ],
    }
  };


  packageManager = {
    packageManager:{
       title:'Package Manager',
       isAtLeastBasic:true,
       key: 'packageManager',
       group:'packageManager',
       children: [
        {order:0,id:'child_0', desc: 'Learn about npm'},
        {order:1,id:'child_1', desc:'Learn about yarn'},
       ],
    }
  };


  cssArchitecture = {
    cssArchitecture:{
       title:'CSS Architecture',
       isAtLeastBasic:true,
       key: 'cssArchitecture',
       group:'cssArchitectureAndPre',
       children: [
        {order:0,id:'child_0', desc: 'Learn about BEM naming convention'},
       ],
    }
  };

  CssPreprocessors = {
    CssPreprocessors:{
       title:'CSS Preprocessors',
       isAtLeastBasic:true,
       key: 'CssPreprocessors',
       group:'cssArchitectureAndPre',
       children: [
        {order:0,id:'child_0', desc: 'Learn SASS'},
        {order:0,id:'child_1', desc: 'Learn PostCSS'},
       ],
    }
  };


frontEndData = {
  ...this.internetBasics,
  ...this.htmlBasics,
  ...this.cssBasics,
  ...this.versionControlSystems,
  ...this.webSecurityKnowledge,
  ...this.packageManager,
  ...this.cssArchitecture,
  ...this.CssPreprocessors
  };

  private PROGRESS_STORAGE = 'progress';

  constructor(private http: HttpClient,) { }


  async loadProgress(){
    const list = await Storage.get({ key: this.PROGRESS_STORAGE });
    this.progress = JSON.parse(list.value) || {};
  }

  saveProgress(){
    Storage.set({
      key: this.PROGRESS_STORAGE,
      value: JSON.stringify(this.progress)
    });
  }


  getGuides(): Observable<any> {
    return this.http.get<any[]>(this.guidesUrl, this.httpOptions)
      .pipe(
        catchError(this.handleError<any[]>('getGuides', []))
      );
  }


    /**
     * Handle Http operation that failed.
     * Let the app continue.
     *
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
     private handleError<T>(operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {

        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead

        // TODO: better job of transforming error for user consumption
        console.log(`${operation} failed: ${error.message}`);

        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }

}
