

import { HttpClient } from '@angular/common/http';

import { Component, OnInit, Inject, Input } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import 'leader-line';
// eslint-disable-next-line @typescript-eslint/naming-convention
declare let LeaderLine: any;
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel, RoutesRecognized } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Output, EventEmitter } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-roadmap',
  templateUrl: 'roadmap.component.html',
  styleUrls: ['roadmap.component.scss']
})
export class RoadmapComponent implements OnInit {
  @Output() clickEvent = new EventEmitter<any>();
  @Output() scrollEvent = new EventEmitter<any>();
  @Output() stopScrollEvent = new EventEmitter<any>();
  @Input() roadmapArr: any[];
  @Input() mainIdPrefix: string;
  @Input() type: string; //frontend, backend,....etc
  showDiagram: boolean;
  line: any;
  lines = [];
  childrenLines = [];
  leftChildren = [];
  rightChildren = [];
  selectedParentId: string;
  typeDictionary = {
    frontend: 'frontEndData',
    backend: 'backEndData',
    devops: 'devOpData'
  };
  public json;

  constructor(
    @Inject(DOCUMENT) private document,
    private http: HttpClient,
    private router: Router,
    private dataService: DataService,
  ) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.destroyLines();
      }
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });

  };

  blockClicked(value: any) {
    this.clickEvent.emit(value);
    this.destroyLines();
  }

  onScroll(event) {
    this.rePositionLines();
    this.scrollEvent.emit();
  }

  onEndScroll(){
    this.stopScrollEvent.emit();
  }

  ngOnInit() {
    this.initiate();
  }

  async initiate() {
    console.log('initiated');
    await this.dataService.loadProgress();
    this.showBadges();
    this.lines = [];
    this.showDiagram = true;

    setTimeout(() => {
      if (this.roadmapArr.length > 0) {
        this.roadmapArr.forEach((e, index) => {
          if (index === this.roadmapArr.length - 1) {
            return;
          }
          const indexSecond = index + 1;
          const first = this.mainIdPrefix + '_' + index;
          const firstElem = document.getElementById(first);
          const second = this.mainIdPrefix + '_' + indexSecond;
          const secondElem = document.getElementById(second);
          const line = new LeaderLine(
            firstElem,
            secondElem
          );
          this.lines.push(line);
        });
      }
    }, 600);
  }

  showBadges() {
    this.roadmapArr.forEach(parent => {
      const booleanArr = [];
      const data = this.dataService[this.typeDictionary[this.type]];
      const parentObject = data[parent.key];
      const childrenArr = parentObject.children;
      childrenArr.forEach(element => {
        const mergedId = parent.id + '_' + element.id;
        const isDone = this.dataService.progress[mergedId] || false;
        booleanArr.push(isDone);
      });
      if (parent.isAtLeastOne) {
        if (booleanArr.some(e => e === true)) {
          parent.status = 'done';
        }
        else {
          parent.status = null;
        }
      }
      else{
        if (booleanArr.every(e => e === true)) {
          parent.status = 'done';
        }
        else if (booleanArr.some(e => e === true)) {
          parent.status = 'onGoing';
        }
        else {
          parent.status = null;
        }
      }
    });
  }

  destroyLines() {
    this.showDiagram = false;
    this.resetLines();
  }


  createChildren(e, arr) {
    this.selectedParentId = e.id;
  }

  rePositionLines() {
    setTimeout(() => {
      if (!this.lines) {
        return;
      }
      if (this.lines.length === 0) {
        return;
      }
      this.lines.forEach(line => {
        line.position();
      });
    }, 10);
  }


  resetLines() {
    this.lines.forEach(line => {
      if (line) {
        line.remove();
      }
    });
    this.lines = [];
  }

}
