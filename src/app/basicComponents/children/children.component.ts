import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss'],
})
export class ChildrenComponent implements OnInit {
 @Input() children = [];
 @Input() parentId;
 colorDic = {
  0:'red',
  1:'navy',
  2:'darkGreen',
  3:'blueishGreen',
  4:'blue',
  5:'blood',
  6:'dark',
  7:'brown',
  8:'darkerGreen'
 };

  constructor(private dataService: DataService) { }
 async save(childId, val){
    const mergedId = this.parentId + '_' + childId;
    this.dataService.progress[mergedId] = val;
    this.dataService.saveProgress();
    await this.dataService.loadProgress();
  }


  ngOnInit() {}

}
